import expressoTradicional from '../assets/coffees/expressoTradicional.svg'
import expressoAmericano from '../assets/coffees/expressoAmericano.svg'
import expressoCremoso from '../assets/coffees/expressoCremoso.svg'
import expressoGelado from '../assets/coffees/expressoGelado.svg'
import cafeComLeite from '../assets/coffees/cafeComLeite.svg'
import latte from '../assets/coffees/latte.svg'
import capuccino from '../assets/coffees/capuccino.svg'
import macchiato from '../assets/coffees/macchiato.svg'

interface Item {
  id: number;
  type: string;
  optional?: string;
  name: string;
  description: string;
  value: number;
  picture: string;
  amount: number;
}

export const items: Item[] = [
  {
    id: 1,
    type: 'TRADICIONAL',
    name: 'Expresso Tradicional',
    description: 'O tradicional café feito com água quente e grãos moídos',
    value: 9.90,
    amount: 0,
    picture: expressoTradicional,
  },
  {
    id: 2,
    type: 'TRADICIONAL',
    name: 'Expresso Americano',
    description: 'Expresso diluído, menos intenso que o tradicional',
    value: 9.90,
    amount: 0,
    picture: expressoAmericano,
  },
  {
    id: 3,
    type: 'TRADICIONAL',
    name: 'Expresso Cremoso',
    description: 'Café expresso tradicional com espuma cremosa',
    value: 9.90,
    amount: 0,
    picture: expressoCremoso,
  },
  {
    id: 4,
    type: 'TRADICIONAL',
    optional: 'GELADO',
    name: 'Expresso Gelado',
    description: 'Bebida preparada com café expresso e cubos de gelo',
    value: 9.90,
    amount: 0,
    picture: expressoGelado,
  },
  {
    id: 5,
    type: 'TRADICIONAL',
    optional: 'COM LEITE',
    name: 'Café com Leite',
    description: 'Meio a meio de expresso tradicional com leite vaporizado',
    value: 9.90,
    amount: 0,
    picture: cafeComLeite,
  },
  {
    id: 6,
    type: 'TRADICIONAL',
    optional: 'COM LEITE',
    name: 'Latte',
    description: 'Uma dose de café expresso com o dobro de leite e espuma cremosa',
    value: 9.90,
    amount: 0,
    picture: latte,
  },
  {
    id: 7,
    type: 'TRADICIONAL',
    optional: 'COM LEITE',
    name: 'Capuccino',
    description: 'Bebida com canela feita de doses iguais de café, leite e espuma',
    value: 9.90,
    amount: 0,
    picture: capuccino,
  },
  {
    id: 8,
    type: 'TRADICIONAL',
    optional: 'COM LEITE',
    name: 'Macchiato',
    description: 'Café expresso misturado com um pouco de leite quente e espuma',
    value: 9.90,
    amount: 0,
    picture: macchiato,
  },
];