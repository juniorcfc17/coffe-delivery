import { items } from "@/utils/coffeesArray";
import { Minus, Plus, ShoppingCartSimple } from "@phosphor-icons/react";
import Image from "next/image";
import { useState, useEffect } from "react";

type CoffeeDataItem = [
  id: number, 
  name: string, 
  picture: string, 
  amount: number
];

export default function Coffee({ setCountCoffee, countCoffee, setStep }: any) {
  const [coffeeItems, setCoffeeItems] = useState(items);
  const [coffeeData, setCoffeeData] = useState<CoffeeDataItem[]>([]);

  useEffect(() => {
    const data = localStorage.getItem("coffeeData");
    if (data) {
      setCoffeeData(JSON.parse(data));
    }
  }, []);

  useEffect(() => {
    localStorage.setItem("coffeeData", JSON.stringify(coffeeData));
  }, [coffeeData]);

  function addAmount(id: number) {
    setCoffeeItems((prevItems) =>
      prevItems.map((item) =>
        item.id === id ? { ...item, amount: item.amount + 1 } : item
      )
    );
    setCountCoffee(countCoffee + 1)
    setCoffeeData((prevData) => {
      const item = coffeeItems.find((item) => item.id === id);
      const existingItemIndex = prevData.findIndex((data) => data[0] === id);
      if (existingItemIndex !== -1) {
        prevData[existingItemIndex][3] += 1;
      } else {
        prevData.push([id, item!.name, item!.picture, 1]);
      }
      const updatedData: CoffeeDataItem[] = [...prevData];
      localStorage.setItem("coffeeData", JSON.stringify(updatedData));
      return updatedData;
    });
  }

  function removeAmount(id: number) {
    setCoffeeItems((prevItems) =>
      prevItems.map((item) =>
        item.id === id && item.amount > 0
          ? { ...item, amount: item.amount - 1 }
          : item
      )
    );
    setCountCoffee(countCoffee - 1)
    setCoffeeData((prevData) => {
      const existingItemIndex = prevData.findIndex((data) => data[id] === id);
      if (existingItemIndex !== -1) {
        prevData[existingItemIndex][3] -= 1;
        if (prevData[existingItemIndex][3] === 0) {
          prevData.splice(existingItemIndex, 1);
        }
      }
      const updatedData: CoffeeDataItem[] = [...prevData];
      localStorage.setItem("coffeeData", JSON.stringify(updatedData));
      return updatedData;
    });
  }

  return (
    <div className="pb-14">
      <h1 className="font-baloo text-fontBaloo32 font-bold text-baseTitle">
        Nossos cafés
      </h1>
      <div className="w-full grid grid-cols-4 gap-8 mt-10">
        {coffeeItems.map((item) => {
          return (
            <div
              key={item.id}
              className="flex flex-col items-center px-6 pb-5 bg-baseCard rounded-ss-lg rounded-ee-lg rounded-se-[36px] rounded-es-[36px]"
            >
              <Image
                className="-mt-6"
                src={item.picture}
                alt=""
                width={120}
                height={120}
              />
              <div className="flex items-center gap-1 mt-3">
                <span className="px-2 py-1 font-roboto text-fontRoboto10 text-yellowDark font-bold bg-yellowLight rounded-full">
                  {item.type}
                </span>
                {item.optional && (
                  <span className="px-2 py-1 font-roboto text-fontRoboto10 text-yellowDark font-bold bg-yellowLight rounded-full">
                    {item.optional}
                  </span>
                )}
              </div>
              <p className="mt-4 font-baloo text-fontBaloo20 text-baseSubtitle font-bold">
                {item.name}
              </p>
              <span className="mt-2 font-roboto text-fontRoboto14 text-baseLabel font-normal text-center leading-lineHeight130">
                {item.description}
              </span>
              <div className="w-full flex gap-2 items-center justify-center mt-8">
                <span className="flex items-center gap-1 font-baloo text-fontBaloo24 text-baseText font-bold">
                  <span className="font-roboto text-fontRoboto14 text-baseText font-normal">
                    R$
                  </span>
                  {item.value}
                </span>
                <div className="flex items-center gap-2 p-2 ml-4 bg-baseButton rounded-md">
                  <Minus
                    onClick={() => removeAmount(item.id)}
                    className={`text-purple hover:text-purpleDark ${item.amount === 0
                        ? "cursor-not-allowed"
                        : "hover:cursor-pointer"
                      }`}
                    size={14}
                    weight="regular"
                  />
                  <span className="font-roboto text-fontRoboto16 text-baseTitle font-normal">
                    {item.amount}
                  </span>
                  <Plus
                    onClick={() => addAmount(item.id)}
                    className="text-purple hover:cursor-pointer hover:text-purpleDark"
                    size={14}
                    weight="regular"
                  />
                </div>
                <div className="p-2 text-white bg-purpleDark hover:cursor-pointer hover:bg-purple rounded-md transition ease-in-out">
                  <ShoppingCartSimple onClick={() => setStep(1)} size={22} weight="fill" />
                </div>
              </div>
            </div>
          );
        })}
      </div>
    </div>
  );
}