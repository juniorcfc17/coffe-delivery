import './globals.css'
import type { Metadata } from 'next'

export const metadata: Metadata = {
  title: 'Coffe Delivery'
}

export default function RootLayout({ children, }: { children: React.ReactNode }) {

  return (
    <html lang="pt-br">
      <head>
        <link rel="preconnect" href="https://fonts.googleapis.com" />
        <link rel="preconnect" href="https://fonts.gstatic.com" crossOrigin='' />
        <link href="https://fonts.googleapis.com/css2?family=Baloo+2:wght@700;800&family=Roboto:wght@400;700&display=swap" rel="stylesheet" />
      </head>
      <body className='w-full h-full flex flex-col items-center antialiased'>
        {children}
      </body>
    </html>
  )
}
