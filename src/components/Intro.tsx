import Image from "next/image";
import coffeeImage from '@/assets/coffeImage.svg'
import { Package, ShoppingCart, Timer, Coffee } from "@phosphor-icons/react";

export default function Intro() {
  return (
    <div className="flex gap-40 justify-between py-24">
      <div className="w-full">
        <h1 className="font-baloo text-fontBaloo48 font-bold text-baseTitle leading-lineHeight130">Encontre o café perfeito para qualquer hora do dia</h1>
        <p className="mt-4 pr-10 font-roboto text-fontRoboto20 font-normal text-baseSubtitle leading-lineHeight130">Com o Coffee Delivery você recebe seu café onde estiver, a qualquer hora</p>

        <div className="h-24 flex gap-6 mt-14">
          <div className="flex flex-col justify-between">
            <div className="flex gap-4 items-center">
              <div className="p-2 bg-yellowDark rounded-full">
                <ShoppingCart className="text-white" size={18} weight="fill" />
              </div>
              <p className="font-roboto text-baseText text-fontRoboto16">Compra simples e segura</p>
            </div>
            <div className="flex gap-4 items-center">
            <div className="p-2 bg-yellow rounded-full">
              <Timer className="text-white" size={18} weight="fill" />
            </div>
              <p className="font-roboto text-baseText text-fontRoboto16">Entrega rápida e restreada</p>
            </div>
          </div>
          <div className="flex flex-col justify-between">
            <div className="flex gap-4 items-center">
            <div className="p-2 bg-baseText rounded-full">
            <Package className="text-white" size={18} weight="fill" />
            </div>
              <p className="font-roboto text-baseText text-fontRoboto16">Embalagem mantém o café intacto</p>
            </div>
            <div className="flex gap-4 items-center">
            <div className="p-2 bg-purple rounded-full">
            <Coffee className="text-white" size={18} weight="fill" />
            </div>
              <p className="font-roboto text-baseText text-fontRoboto16">O café chega fresquinho até você</p>
            </div>
          </div>
        </div>

      </div>
      <div className="w-full">
        <Image src={coffeeImage} alt="" />
      </div>
    </div>
  )
}