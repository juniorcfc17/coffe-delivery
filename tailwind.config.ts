import type { Config } from 'tailwindcss'

const config: Config = {
  content: [
    './src/pages/**/*.{js,ts,jsx,tsx,mdx}',
    './src/components/**/*.{js,ts,jsx,tsx,mdx}',
    './src/app/**/*.{js,ts,jsx,tsx,mdx}',
  ],
  theme: {
    extend: {
      backgroundImage: {
        'gradient-radial': 'radial-gradient(var(--tw-gradient-stops))',
        'gradient-conic':
          'conic-gradient(from 180deg at 50% 50%, var(--tw-gradient-stops))',
      },

      colors: {
        yellowDark: '#C47F17',
        yellow: '#DBAC2C',
        yellowLight: '#F1E9C9',

        purpleDark: '#4B2995',
        purple: '#8047F8',
        purpleLight: '#EBE5F9',

        baseTitle: '#272221',
        baseSubtitle: '#403937',
        baseText: '#574F4D',
        baseLabel: '#8D8686',
        baseHover: '#D7D5D5',
        baseButton: '#E6E5E5',
        baseInput: '#EDEDED',
        baseCard: '#F3F2F2',
        white: '#FFFFFF',
      },
      fontFamily: {
        roboto: ['Roboto', 'sans-serif'],
        baloo:  ['"Baloo 2"', 'sans-serif'],
      },
      fontSize: {
        fontBaloo18: '18px',
        fontBaloo20: '20px',
        fontBaloo24: '24px',
        fontBaloo32: '32px',
        fontBaloo48: '48px',

        fontRoboto10: '10px',
        fontRoboto12: '12px',
        fontRoboto14: '14px',
        fontRoboto16: '16px',
        fontRoboto18: '18px',
        fontRoboto20: '20px',
        fontRoboto24: '24px',
      },
      lineHeight: {
        lineHeight130: '130%',
        lineHeight160: '160%',
      }
    },
  },
  plugins: [],
}
export default config
