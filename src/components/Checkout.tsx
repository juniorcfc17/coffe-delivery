import { items } from "@/utils/coffeesArray";
import { Bank, CreditCard, CurrencyDollar, MapPin, Minus, Money, Plus } from "@phosphor-icons/react";
import Image from "next/image";
import { useEffect, useState } from "react";

type CoffeeDataItem = [
  id: number, 
  name: string, 
  picture: string, 
  amount: number
];

export default function Checkout({ setCountCoffee, countCoffee, setStep }: any) {
  const [coffeeData, setCoffeeData] = useState<CoffeeDataItem[]>([]);
  const [coffeeItems, setCoffeeItems] = useState(items);

  useEffect(() => {
    const data = localStorage.getItem("coffeeData");
    if(data) {
      setCoffeeData(JSON.parse(data))
    }
  }, [])

  function addAmount(id: number) {
    setCoffeeItems((prevItems) =>
      prevItems.map((item) =>
        item.id === id ? { ...item, amount: item.amount + 1 } : item
      )
    );
    setCountCoffee(countCoffee + 1)
    setCoffeeData((prevData) => {
      const item = coffeeItems.find((item) => item.id === id);
      const existingItemIndex = prevData.findIndex((data) => data[0] === id);
      if (existingItemIndex !== -1) {
        prevData[existingItemIndex][3] += 1;
      } else {
        prevData.push([id, item!.name, item!.picture, 1]);
      }
      const updatedData: CoffeeDataItem[] = [...prevData];
      localStorage.setItem("coffeeData", JSON.stringify(updatedData));
      return updatedData;
    });
  }

  function removeAmount(id: number) {
    setCoffeeItems((prevItems) =>
      prevItems.map((item) =>
        item.id === id && item.amount > 0
          ? { ...item, amount: item.amount - 1 }
          : item
      )
    );
    setCountCoffee(countCoffee - 1)
    setCoffeeData((prevData) => {
      const existingItemIndex = prevData.findIndex((data) => data[id] === id);
      if (existingItemIndex !== -1) {
        prevData[existingItemIndex][3] -= 1;
        if (prevData[existingItemIndex][3] === 0) {
          prevData.splice(existingItemIndex, 1);
        }
      }
      const updatedData: CoffeeDataItem[] = [...prevData];
      localStorage.setItem("coffeeData", JSON.stringify(updatedData));
      return updatedData;
    });
  }

  return (
    <div className="w-full flex gap-6 mt-10">

      <div className="w-full">
        <h1 className="font-baloo text-fontBaloo18 font-bold text-baseTitle">Complete seu pedido</h1>
        <div className="w-full flex flex-col mt-4 p-10 bg-baseCard rounded-lg ">
          <header className="flex gap-2">
            <MapPin className="text-yellowDark" size={22} />
            <div className="flex flex-col leading-lineHeight130">
              <p className="font-roboto text-fontRoboto16 font-normal text-baseSubtitle">Endereço de Entrega</p>
              <span className="font-roboto text-fontRoboto14 font-normal text-baseText ">Informe o endereço onde deseja receber o seu pedido</span>
            </div>
          </header>

          <input className="w-[200px] mt-8 px-3 py-[0.70rem] font-roboto text-fontRoboto14 text-baseText bg-baseInput border border-baseButton rounded-md"
            type="text" placeholder="CEP" />
          <input className="mt-4 px-3 py-[0.70rem] bg-baseInput border border-baseButton rounded-md"
            type="text" placeholder="Rua" />

          <div className="flex gap-3 mt-4">
            <input className="w-[200px] p-3 font-roboto text-fontRoboto14 text-baseText bg-baseInput border border-baseButton rounded-md"
              type="text" placeholder="Número" />
            <div className="flex">
              <input className="w-[337px] p-3 font-roboto text-fontRoboto14 text-baseText bg-baseInput border border-baseButton rounded-md"
                type="text" placeholder="Complemento"
              />
              <span className="z-10 -ml-16 mt-[14px] font-roboto text-fontRoboto12 italic font-normal text-baseLabel">Opcional</span>
            </div>
          </div>
          <div className="flex gap-3 mt-4">
            <input className="w-[200px] p-3 font-roboto text-fontRoboto14 text-baseText bg-baseInput border border-baseButton rounded-md"
              type="text" placeholder="Bairro" />
            <input className="w-full p-3 font-roboto text-fontRoboto14 text-baseText bg-baseInput border border-baseButton rounded-md"
              type="text" placeholder="Cidade" />
            <input className="w-[60px] p-3 font-roboto text-fontRoboto14 text-baseText bg-baseInput border border-baseButton rounded-md"
              type="text" placeholder="UF" />
          </div>
        </div>

        <div className="w-full flex flex-col mt-4 p-10 bg-baseCard rounded-lg">
          <header className="flex gap-2">
            <CurrencyDollar className="text-purple" size={22} />
            <div className="flex flex-col leading-lineHeight130">
              <p className="font-roboto text-fontRoboto16 font-normal text-baseSubtitle">Pagamento</p>
              <span className="font-roboto text-fontRoboto14 font-normal text-baseText ">O pagamento é feito na entrega. Escolha a forma que deseja pagar</span>
            </div>
          </header>

          <div className="grid grid-cols-3 gap-3 mt-12">
            <button className="w-full flex gap-1 items-center p-4 font-roboto text-fontRoboto12 text-baseText bg-baseButton hover:bg-baseHover focus:ring-1 focus:ring-purple focus:bg-purpleLight transition ease-in-out border border-baseButton rounded-md">
              <CreditCard className="text-purple" size={22} />
              <span>CARTÃO DE CRÉDITO</span>
            </button>
            <button className="w-full flex gap-1 items-center p-4 font-roboto text-fontRoboto12 text-baseText bg-baseButton hover:bg-baseHover focus:ring-1 focus:ring-purple focus:bg-purpleLight transition ease-in-out border border-baseButton rounded-md">
              <Bank className="text-purple" size={22} />
              <span>CARTÃO DE DÉBITO</span>
            </button>
            <button className="w-full flex gap-1 items-center p-4 font-roboto text-fontRoboto12 text-baseText bg-baseButton hover:bg-baseHover focus:ring-1 focus:ring-purple focus:bg-purpleLight transition ease-in-out border border-baseButton rounded-md">
              <Money className="text-purple" size={22} />
              <span className="ml-3">DINHEIRO</span>
            </button>
          </div>
        </div>
      </div>

      <div className="w-full">
        <h1 className="font-baloo text-fontBaloo18 font-bold text-baseTitle">CaFés selecionados</h1>

        <div className="flex flex-col items-center mt-4 p-10 pb-5 bg-baseCard rounded-ss-lg rounded-ee-lg rounded-se-[36px] rounded-es-[36px]">
          {coffeeData.map((item) => {
            return (
              <div key={item[0]} className="flex gap-2">
                <Image src={item[2]} alt=""/>
                <div>
                  <span>{item[1]}</span>
                  <div className="flex items-center gap-2 p-2 ml-4 bg-baseButton rounded-md">
                  <Minus
                    onClick={() => removeAmount(item[0])}
                    className={`text-purple hover:text-purpleDark ${item[3] === 0
                        ? "cursor-not-allowed"
                        : "hover:cursor-pointer"
                      }`}
                    size={14}
                    weight="regular"
                  />
                  <span className="font-roboto text-fontRoboto16 text-baseTitle font-normal">
                    {item[3]}
                  </span>
                  <Plus
                    onClick={() => addAmount(item[0])}
                    className="text-purple hover:cursor-pointer hover:text-purpleDark"
                    size={14}
                    weight="regular"
                  />
                </div>
                </div>
                <span>{item[2]}</span>
              </div>
            );
          })}
        </div>
      </div>

    </div>
  )
}