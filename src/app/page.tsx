'use client'
import Image from 'next/image'
import logoImage from '@/assets/coffeLogo.svg'
import { MapPin, ShoppingCart } from '@phosphor-icons/react'
import Intro from '@/components/Intro'
import Coffee from '@/components/Coffee'
import { useState } from 'react'
import Checkout from '@/components/Checkout'

export default function Home() {
  const [countCoffee, setCountCoffee] = useState<number>(0)
  const [step, setStep] = useState<number>(0)

  return (
    <div className='w-full max-w-7xl h-screen '>
      <header className='flex justify-between py-8'>
        <Image src={logoImage} alt=''/>
        <div className='flex gap-2'>
          <span className='flex items-center gap-1 px-3 py-2 bg-purpleLight rounded-md'>
            <MapPin className='text-purple' size={22} weight='fill'/>
            <p className='text-fontRoboto14 text-purple font-roboto'>Curitiba, PR</p>
          </span>
          <div onClick={() => setStep(1)} className='p-2 text-yellowDark bg-yellowLight rounded-md hover:cursor-pointer hover:bg-yellowDark hover:text-yellowLight transition ease-in-out'>
            <ShoppingCart size={22} weight='fill' />
          </div>
            {countCoffee > 0 &&
              <span className='w-5 h-5 flex items-center justify-center -ml-5 -mt-2 font-roboto text-white text-fontRoboto12 text-center font-bold bg-yellowDark rounded-full'>{countCoffee}</span>
            }
        </div>
      </header>
      
      <div className='w-full'>
        {step === 0 &&
        <>
          <Intro />
          <Coffee setCountCoffee={setCountCoffee} countCoffee={countCoffee} setStep={setStep}/>
        </>
        }
        {step === 1 &&
          <Checkout setCountCoffee={setCountCoffee} countCoffee={countCoffee} setStep={setStep}/>
        }
      </div>
    </div>
  )
}
